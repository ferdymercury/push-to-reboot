/*
 * Memory efficient HMAC-SHA1 implementation.
 * This implementation is based loosely on the example given in RFC 2104 with
 * several modifications to reduce memory usage.
 * See https://tools.ietf.org/html/rfc2104 for details.
 *
 * This module can be used two ways: to hash a single message or to hash
 * several messages as if concatenated as a single message. Both of the
 * following examples produce the same digest.
 *
 * To computing the hash of a single message:
 *
 *    const uint8_t key[] = "password123";
 *    const size_t key_length = 11;
 *    const uint8_t message[] = "This is the message.";
 *    const size_t message_length = 20;
 *    uint8_t digest[SHA1_HASH_SIZE];
 *
 *    hmac_sha1_hash(key, key_length, message, message_length, digest);
 *
 * To compute the hash of multiple messages concatenated:
 *
 *    const uint8_t key[] = "password123";
 *    const size_t key_length = 11;
 *    const uint8_t message_a[] = "This is ";
 *    const size_t message_a_length = 8;
 *    const uint8_t message_b[] = "the message.";
 *    const size_t message_b_length = 12;
 *    hmac_sha1_context context;
 *    uint8_t digest[SHA1_HASH_SIZE];
 *
 *    hmac_sha1_init(&context, key, key_length);
 *    hmac_sha1_update(&context, message_a, message_a_length);
 *    hmac_sha1_update(&context, message_b, message_b_length);
 *    hmac_sha1_digest(&context, digest);
 */

#ifndef HMAC_SHA1_H
#define HMAC_SHA1_H

#include "sha1.h"

#include <stddef.h>
#include <stdint.h>

/*
 * Size of a message digest in octets.
 */
#define HMAC_SHA1_HASH_SIZE SHA1_HASH_SIZE


/*
 * State used when hashing multiple concatenated messages.
 */
typedef struct hmac_sha1_context {
    sha1_context context;               // hash context
    uint8_t block_key[SHA1_BLOCK_SIZE]; // block-sized key
} hmac_sha1_context;

/*
 * Set the initial state of a context.
 * This must be called before any other functions that accept a context.
 *
 * context: context to initialize
 * key: array of octets used to make inner and outer keys
 * length: count of octets in `key`
 */
void hmac_sha1_init(hmac_sha1_context *context,
        const uint8_t *key, size_t length);

/*
 * Update a context with a message.
 * This must be called once for each message being concatenated and cannot be
 * called until `context` has been initialized with `hmac_sha1_init()`.
 *
 * context: context to update
 * message: array of octets to hash
 * length: count of octets in `message`
 */
void hmac_sha1_update(hmac_sha1_context *context,
        const uint8_t *message, size_t length);

/*
 * Calculate the digest of all messages added to a context.
 * This must be called after the final call of `hmac_sha1_update()`. After calling
 * this function `context` must be discarded or reset using `hmac_sha1_init()`
 * before being reused.
 *
 * context: context to update
 * digest: hash output
 */
void hmac_sha1_digest(hmac_sha1_context *context, uint8_t *digest);

/*
 * Calculate the HMAC-SHA1 hash of a message.
 *
 * key: array of octets used to make inner and outer keys
 * key_length: count of octets in `key`
 * message: array of octets to hash
 * message_length: count of octets in `message`
 * digest: hash output
 */
void hmac_sha1_hash(const uint8_t *key, size_t key_length,
        const uint8_t *message, size_t message_length,
        uint8_t *digest);


#endif /* HMAC_SHA1_H */
