---
layout: page
index: 1
title: Node Construction
description: How to assemble a node using hardware modules
---

# Node Construction
The current hardware is built into a power strip on protoboard using an Arduino
Nano clone.

## Schematic
**[TODO]**

## Materials
**[TODO]**

## Assembly
**[TODO]**

### DTMF Module Modifications
**[TODO]**

### Relay Module Modifications
**[TODO]**

### Radio Installation
Use two 2.5mm to 3.5mm TRS audio cables to connect the headset ports on
the radio to the audio ports on the power strip. The 3.5mm jack on the radio
will be connected to the 2.5mm jack on the power strip and the 2.5mm jack on
the radio will be connected to the 3.5mm jack on the power strip.

**[TODO connection diagram]**
