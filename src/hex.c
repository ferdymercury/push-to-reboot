/*
 * Hexadecimal (base 16) decoder.
 */

#include "hex.h"

#include <ctype.h>
#include <stdbool.h>


/*
 * Decode a single hex character into a byte.
 *
 * character: hex character
 * value: pointer to output byte
 * return: true if decoding was succesful, false otherwise
 */
bool decode_quartet(char character, uint8_t *value) {
    character = toupper(character);

    if (character >= '0' && character <= '9') {
        *value = character - '0';
        return true;
    }
    if (character >= 'A' && character <= 'F') {
        *value = character - 'A' + 10;
        return true;
    }

    return false;
}

/*
 * Decode 2 hex characters into a byte.
 *
 * high_character: first character of a hex pair
 * low_character: second character of a hex pair
 * value: pointer to output byte
 * return: true if decoding was succesful, false otherwise
 */
bool decode_octet(char high_character, char low_character, uint8_t *value) {
    uint8_t high_value;
    uint8_t low_value;

    if (!decode_quartet(high_character, &high_value)) {
        return false;
    }
    if (!decode_quartet(low_character, &low_value)) {
        return false;
    }

    *value = (high_value << 4) | low_value;

    return true;
}

/* Decode a hexadecimal string to an array of bytes. */
hex_error hex_decode(
        const char *string, uint8_t *bytes, size_t bytes_length,
        size_t *byte_count) {
    *byte_count = 0; // reset count of decoded bytes

    // process two quartets at a time
    for (size_t i = 0; string[i] != '\0'; i += 2) {
        uint8_t octet_value;

        // fail if output buffer full
        if (*byte_count == bytes_length) {
            return hex_error_output_full;
        }
        // fail if character invalid
        if (!decode_octet(string[i], string[i + 1], &octet_value)) {
            return hex_error_invalid_input;
        }

        bytes[*byte_count] = octet_value;
        ++*byte_count; // move to next octet
    }

    return hex_error_success;
}
