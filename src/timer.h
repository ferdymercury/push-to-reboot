/*
 * Second-resolution timer operations using TIMER1.
 */

#ifndef TIMER_H
#define TIMER_H

#include <stdint.h>


/*
 * Set overflow interval and start timer.
 * This function needs to be called before any other in the same module.
 */
void timer_init(void);

/*
 * Set timer to update uptime at 1 millisecond intervals.
 */
void timer_init(void);

/*
 * Get the number of elapsed milliseconds since timer initialization.
 *
 * return: elapsed milliseconds since timer initialization
 */
uint32_t timer_uptime(void);

/*
 * Delay for the specified amount of milliseconds.
 *
 * milliseconds: number of milliseconds to block for
 */
void timer_delay_millis(uint32_t milliseconds);

/*
 * Delay for the specified amount of seconds.
 *
 * seconds: number of seconds to block for
 */
void timer_delay_secs(uint32_t seconds);


#endif /* TIMER_H */
