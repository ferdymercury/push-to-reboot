/*
 * HMAC-based one-time password algorithm.
 * This implementation is based on RFC 4226.
 * See https://tools.ietf.org/html/rfc4226 for details.
 *
 * This module can be used two ways: to generate one-time passwords, or to
 * check a one-time password.
 *
 * To generate a one-time password:
 *
 *    const uint8_t secret[] = "12345678901234567890";
 *    const size_t secret_length = 20;
 *    hotp_context context;
 *    char password[HOTP_PASSWORD_LENGTH + 1];
 *
 *    hotp_init(&context, secret, secret_length);
 *    hotp_next(&context, password);
 *
 * To check a one-time password:
 *
 *    const uint8_t secret[] = "12345678901234567890";
 *    const size_t secret_length = 20;
 *    const char *password = "755224";
 *    hotp_context context;
 *    bool success;
 *
 *    hotp_init(&context, secret, secret_length);
 *    success = hotp_check(&context, password);
 */

#ifndef HOTP_H
#define HOTP_H

#include "hmac-sha1.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/*
 * Maximum size of a secret in octets.
 */
#define HOTP_SECRET_SIZE 32

/*
 * Count of digits in a one-time password.
 */
#define HOTP_PASSWORD_LENGTH 6

/*
 * Count of passwords to check for a match.
 * 25 is the maximum value recommended by Yubico: https://www.yubico.com
 * /wp-content/uploads/2016/02/YubicoBestPracticesOATH-HOTP.pdf
 */
#define HOTP_LOOKAHEAD_COUNT 25


/*
 * State used when generating or checking one-time passwords.
 */
typedef struct hotp_context {
    uint8_t secret[HOTP_SECRET_SIZE];   // secret used when hashing counter
    size_t secret_length;               // length of secret in octets
    uint64_t counter;                   // 8-byte counter used as moving factor
} hotp_context;

/*
 * Set the secret of a context and reset the counter to 0.
 *
 * context: context to initialize
 * secret: secret used when hashing counter
 * length: length of secret in octets
 */
void hotp_init(hotp_context *context,
        const uint8_t *secret, size_t length);

/*
 * Get the next password in the sequence and increment counter.
 *
 * context: context to advance
 * password: destination for password
 */
void hotp_next(hotp_context *context, char *password);

/*
 * Check if a password is within the synchronization window.
 * If `password` is found `context`'s counter will be advanced to the index
 * after the found password. Up to `HOTP_LOOKAHEAD_COUNT` passwords will be
 * generated and checked when this function is called.
 *
 * context: context to advance
 * password: password to check
 * return: true if `pasword` is found; false otherwise
 */
bool hotp_check(hotp_context *context, const char *password);


#endif /* HOTP_H */
