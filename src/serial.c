/*
 * Async serial using hardware USART.
 */

#include "serial.h"
#include "timer.h"

#include <avr/io.h>

/*
 * Baud rate of serial communications.
 */
#define BAUD 9600UL
#include <util/setbaud.h>

#include <ctype.h>
#include <stddef.h>


/* Set speed and mode of the UART. */
void serial_init(void) {
    // set rate using macros defined by setbaud.h
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;

#if USE_2X
    UCSR0A |= _BV(U2X0);
#else
    UCSR0A &= ~(_BV(U2X0));
#endif

    // set serial mode and enable
    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); // async 8n1
    UCSR0B = _BV(TXEN0) | _BV(RXEN0); // enable transmit and recieve
}

/* Indicate if a character is available to be read. */
bool serial_available(void) {
    return bit_is_set(UCSR0A, RXC0);
}

/* Read a single character from the UART. */
char serial_read(uint32_t timeout) {
    uint32_t start_time;

    start_time = timer_uptime();
    while ((timer_uptime() - start_time) < timeout) {
        // byte has arrived
        if (bit_is_set(UCSR0A, RXC0)) {
            return UDR0;
        }
    }

    // return NUL on timeout
    return '\0';
}

/* Read a '\r' delimited string of characters from the UART. */
size_t serial_read_line(char *buffer, size_t length, uint32_t timeout) {
    size_t character_count = 0;

    while (true) {
        char character;

        character = serial_read(timeout);
        switch (character) {
            // backspace
            case '\b':
                if (character_count != 0) {
                    --character_count;
                    // erase old character
                    serial_write_string("\b \b");
                }
                break;
            // carriage return
            case '\r':
                goto eol;
            // timeout
            case '\0':
                goto error;
            // all other characters
            default:
                // discard control characters
                if (iscntrl(character)) {
                    break; 
                }
                // detect overflow
                if (character_count == length - 1) {
                    goto error;
                }

                buffer[character_count++] = character; // copy to buffer
                serial_write(character); // echo
                break;
        }
    }

error:
    character_count = 0; // truncate buffer
eol:
    buffer[character_count] = '\0'; // terminate buffer
    serial_write_newline(); // move cursor to next line

    return character_count;
}

/* Write a single char to the UART. */
void serial_write(char data) {
    loop_until_bit_is_set(UCSR0A, UDRE0); // wait for empty transmit buffer
    UDR0 = data;
}

/* Write a string to the UART. */
void serial_write_string(const char *data) {
    while (*data != '\0') {
        serial_write(*data);
        ++data;
    }
}

/* Write "\r\n" to the UART. */
void serial_write_newline(void) {
    serial_write('\r');
    serial_write('\n');
}

/* Write a string followed by "\r\n" to the UART. */
void serial_write_line(const char *data) {
    serial_write_string(data);
    serial_write_newline();
}

/* Write a string from program memory to the UART. */
void serial_write_p_string(PGM_P data) {
    char character;

    character = pgm_read_byte(data);
    while (character != '\0') {
        serial_write(character);
        character = pgm_read_byte(++data);
    }
}

/* Write a string followed by "\r\n" from program memory to the UART. */
void serial_write_p_line(PGM_P data) {
    serial_write_p_string(data);
    serial_write_newline();
}
