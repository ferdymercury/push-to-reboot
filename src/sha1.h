/*
 * Memory efficient SHA1 implementation.
 * This implementation is based on section 6.2 "Method 2" of RFC 3174.
 * See https://tools.ietf.org/html/rfc3174#section-6.2 for details.
 *
 * This module can be used two ways: to hash a single message or to hash
 * several messages as if concatenated as a single message. Both of the
 * following examples produce the same digest.
 *
 * To computing the hash of a single message:
 *
 *    const uint8_t message[] = "This is the message.";
 *    const size_t message_length = 20;
 *    uint8_t digest[SHA1_HASH_SIZE];
 *
 *    sha1_hash(message, message_length, digest);
 *
 * To compute the hash of multiple messages concatenated:
 *
 *    const uint8_t message_a[] = "This is ";
 *    const size_t message_a_length = 8;
 *    const uint8_t message_b[] = "the message.";
 *    const size_t message_b_length = 12;
 *    sha1_context context;
 *    uint8_t digest[SHA1_HASH_SIZE];
 *
 *    sha1_init(&context);
 *    sha1_update(&context, message_a, message_a_length);
 *    sha1_update(&context, message_b, message_b_length);
 *    sha1_digest(&context, digest);
 */

#ifndef SHA1_H
#define SHA1_H

#include <stddef.h>
#include <stdint.h>

/*
 * Size of a message digest in octets.
 */
#define SHA1_HASH_SIZE 20

/*
 * Size of a message block in octets.
 */
#define SHA1_BLOCK_SIZE 64


/*
 * State used when hashing multiple concatenated messages.
 */
typedef struct sha1_context {
    uint8_t block[SHA1_BLOCK_SIZE]; // block of message data
    size_t block_index;             // index of last data block
    uint32_t intermediate[5];       // intermediate digest
    uint64_t bit_length;            // bit length of processed message
} sha1_context;

/*
 * Set the initial state of a context.
 * This must be called before any other functions that accept a context.
 *
 * context: context to initialize
 */
void sha1_init(sha1_context *context);

/*
 * Update a context with a message.
 * This must be called once for each message being concatenated and cannot be
 * called until `context` has been initialized with `sha1_init()`.
 *
 * context: context to update
 * message: array of octets to hash
 * length: count of octets in message
 */
void sha1_update(sha1_context *context, const uint8_t *message, size_t length);

/*
 * Calculate the digest of all messages added to a context.
 * This must be called after the final call of `sha1_update()`. After calling
 * this function `context` must be discarded or reset using `sha1_init()`
 * before being reused.
 *
 * context: context to update
 * digest: hash output
 */
void sha1_digest(sha1_context *context, uint8_t *digest);

/*
 * Calculate the SHA1 hash of a message.
 *
 * message: array of octets to hash
 * length: count of octets in `message`
 * digest: hash output
 */
void sha1_hash(const uint8_t *message, size_t length, uint8_t *digest);


#endif /* SHA1_H */
