# Push to Reboot
A controllable power strip that utilizes off-the-shelf analog two-way radios
for long range communication. This repository contains the Push to Reboot
firmware source, hardware schematic, and project documentation. See the
[Push to Reboot project page](https://hackaday.io/project/134298-push-to-reboot)
for information about how this project was made.


## Documentation
See the [documentation](https://unixispower.gitlab.io/push-to-reboot/) for
detailed information on the PTR protocol, how to assemble and program hardware,
and how to set up a handheld radio to control power strips. 

### Building
Documentation is built using [Jekyll](https://jekyllrb.com/) and hosted using
[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/). Locally
building the documentation requires [Bundler](https://bundler.io/).

To install documentation build prerequisites run:
```shell
$ bundle install --gemfile=docs/Gemfile
```

To generate HTML to `docs/_site` and start a local webserver run: 

```shell
$ make docs
```


## Hardware
The node hardware is built using an Arduino Nano clone that uses an
ATmega328P. The `hw` directory contains the schematic files for
[Kicad](http://kicad-pcb.org/).


## Firmware
Node hardware is built using an Arduino clone, but the firmware is written
in C99 instead of the Arduino programming language. No dependencies besides
`avc-libc` are used.

### Building
The firmware is built using the [GNU AVR Toolchain](
http://www.nongnu.org/avr-libc/user-manual/overview.html). To build the ELF,
flash HEX, and EEPROM HEX to the `bin` directory run:

```shell
$ make
```

### Testing
Modules that are portable can be natively compiled and tested. A separate
makefile `Makefile.test` is responsible for compiling and executing all tests
in the `test` directory. The test makefile can be executed from the primary
makefile `Makefile` by running the following:

```shell
$ make test
```

### Programming
The built firmware and EEPROM images are programmed to the ATmega328 using
[AVRDUDE](http://www.nongnu.org/avrdude/). By default the makefile is set to
use the AVRISP mkII; to change this edit the `PROGRAMMER` variable in the
`Makefile`. To flash the firmware and write the EEPROM connect the
ISP then run:

```shell
$ make program
```


## Licensing
Source in this repository is licensed under the 2-clause BSD license, see
`LICENSE` for details.
