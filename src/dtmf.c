/*
 * Driver for MT8870 DTMF decoder board.
 */

#include "dtmf.h"
#include "timer.h"

#include <avr/io.h>

/*
 * Port where MT8870 is connected.
 */
#define DTMF_DDR DDRD
#define DTMF_PORT PORTD
#define DTMF_PIN PIND

/*
 * Mask for accessing data lines.
 * High 4 bits of port are Q1-Q4 (PD4-PD7).
 */
#define DTMF_Q_MASK 0xF0
#define DTMF_STD_BIT PD3


/*
 * Decode a single DTMF character from the state of Q1-Q4.
 *
 * state: nibble read from Q lines of decoder
 * return: the decoded DTMF digit
 */
char decode_q(uint8_t state) {
    switch (state) {
        case 1:  return '1';
        case 2:  return '2';
        case 3:  return '3';
        case 4:  return '4';
        case 5:  return '5';
        case 6:  return '6';
        case 7:  return '7';
        case 8:  return '8';
        case 9:  return '9';
        case 10: return '0';
        case 11: return '*';
        case 12: return '#';
        case 13: return 'A';
        case 14: return 'B';
        case 15: return 'C';
        case 0:  return 'D';
        default: return 0; // quiet compiler warning
    }
}

/* Indicate if a character is available to be read. */
void dtmf_init(void) {    
    DTMF_DDR &= !(DTMF_Q_MASK | _BV(DTMF_STD_BIT)); // set Q1-Q4, StD as input
    DTMF_PORT |= DTMF_Q_MASK | _BV(DTMF_STD_BIT); // enable pullups
}

/* Indicate if a character is available to be read. */
bool dtmf_available(void) {
    return bit_is_set(DTMF_PIN, DTMF_STD_BIT);
}

/* Read a single character from the DTMF decoder. */
char dtmf_read(uint32_t timeout) {
    uint32_t start_time;

    start_time = timer_uptime();
    while ((timer_uptime() - start_time) < timeout) {
        // character has arrived
        if (bit_is_set(DTMF_PIN, DTMF_STD_BIT)) {
            uint8_t q_state;
            char digit;

            q_state = (DTMF_PIN & DTMF_Q_MASK) >> 4;
            digit = decode_q(q_state);

            // wait for digit to end so subsequent function calls don't
            // detect the same digit that started on the previous call
            loop_until_bit_is_clear(DTMF_PIN, DTMF_STD_BIT);
            return digit;
        }
    }

    // return NUL on timeout
    return '\0';
}

/* Read a delimited string of characters from the DTMF decoder. */
size_t dtmf_read_line(char *buffer, size_t length, uint32_t timeout) {
    size_t character_count = 0;

    // wait for beginning of line
    while (true) {
        char character;

        character = dtmf_read(timeout);
        switch (character) {
            // timeout
            case '\0':
                goto error;
            // beginning of line
            case '*':
                break;
        }
    }

    // read remainder of line
    while (true) {
        char character;

        character = dtmf_read(timeout);
        switch (character) {
            // clear buffer
            case '*':
                character_count = 0;
                break;
            // end of line
            case '#':
                goto eol;
            // timeout
            case '\0':
                goto error;
            // all other characters
            default:
                // detect overflow
                if (character_count == length - 1) {
                    goto error;
                }

                buffer[character_count++] = character;
                break;
        }
    }

error:
    character_count = 0; // truncate buffer
eol:
    buffer[character_count] = '\0'; // terminate buffer

    return character_count;
}
