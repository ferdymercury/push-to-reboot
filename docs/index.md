---
layout: default
---

# Coming Soon
Push to reboot (PTR) is a controllable power strip that utilizes off-the-shelf
analog two-way radios for long range communication. The full PTR documentation
will be available soon.
