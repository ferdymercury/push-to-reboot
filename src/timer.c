/*
 * Millisecond-resolution timer operations using TIMER1.
 */

#include "timer.h"

#include <avr/io.h>
#include <avr/interrupt.h>


/* Number of milliseconds since timer initialization */
volatile uint32_t uptime = 0;

/* Increment uptime on every timer overflow */
ISR(TIMER1_COMPA_vect) {
    ++uptime;
}

/* Set overflow interval and start timer. */
void timer_init(void) {
    cli();
    OCR1A = F_CPU / 1000U - 1; // set to overflow at 1 millisecond
    TCCR1B = _BV(WGM12) | _BV(CS10); // start timer in CTC mode / no prescaler
    TIMSK1 |= _BV(OCIE1A); // enable interrupt on timer overflow
    sei();
}

/* Get the number of elapsed milliseconds since timer initialization. */
uint32_t timer_uptime(void) {
    uint32_t uptime_value;

    // disabling interrupts is required because reading a uint32_t isn't atomic
    cli();
    uptime_value = uptime;
    sei();

    return uptime_value;
}

/* Delay for the specified amount of milliseconds. */
void timer_delay_millis(uint32_t milliseconds) {
    uint32_t start_time;

    start_time = timer_uptime();
    while ((timer_uptime() - start_time) < milliseconds);
}

/* Delay for the specified amount of seconds. */
void timer_delay_secs(uint32_t seconds) {
    timer_delay_millis(seconds * 1000U);
}
