/*
 * Base 32 decoder.
 */

#include "base32.h"

#include <ctype.h>
#include <stdbool.h>

#define PAD_CHARACTER '='


/*
 * Decode a base 32 character into a byte with the 5 lowest bits set.
 *
 * character: quintet character to decode
 * value: pointer to output byte
 * return: true if decoding was succesful, false otherwise
 */
bool decode_quintet(char character, uint8_t *value) {
    character = toupper(character);

    if (character >= 'A' && character <= 'Z') {
        *value = character - 'A';
        return true;
    }
    if (character >= '2' && character <= '7') {
        *value = character - '2' + 26;
        return true;
    }

    return false;
}

/* Decode a base 32 string to an array of bytes. */
base32_error base32_decode(
        const char *string, uint8_t *bytes, size_t bytes_length,
        size_t *byte_count) {
    uint8_t shift_count = 0; // number of bits to right shift a quintet

    *byte_count = 0; // reset count of decoded bytes

    // process one quintet at a time
    for (size_t i = 0; string[i] != '\0'; ++i) {
        char quintet_character;
        uint8_t quintet_value;

        quintet_character = string[i];

        // terminate if padding found
        if (quintet_character == PAD_CHARACTER) {
            return base32_error_success;
        }
        // fail if output buffer full
        if (*byte_count == bytes_length) {
            return base32_error_output_full;
        }
        // fail if character invalid
        if (!decode_quintet(quintet_character, &quintet_value)) {
            return base32_error_invalid_input;
        }

        // clear first octet of 40 bit group
        if (shift_count == 0) {
            bytes[*byte_count] = 0x00;
        }

        // copy beginning of quintet to current octet
        quintet_value <<= 3; // "left justify" bits
        bytes[*byte_count] |= quintet_value >> shift_count;

        // copy remainder of quintet to next octet
        if (shift_count > 3) {
            ++*byte_count; // move to next octet

            // fail if output buffer full
            if (*byte_count == bytes_length) {
                return base32_error_output_full;
            }

            // place end of quintet at beginning of octet
            bytes[*byte_count] = quintet_value << (8 - shift_count);
        }

        // calculate shift for next quintet
        shift_count = (shift_count + 5) % 8;

        // advance to next octet when a new 40 bit group begins
        if (shift_count == 0) {
            ++*byte_count; // move to next octet
        }
    }

    return base32_error_success;
}
