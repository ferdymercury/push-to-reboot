/*
 * Memory efficient HMAC-SHA1 implementation.
 */

#include "hmac-sha1.h"

#include <string.h>


/*
 * Convert a variable length key to a block-length key.
 *
 * key: array of octets to derive `block_key` from
 * length: count of octets in `key`
 * block_key: block-length array of octets containing converted key
 */
void make_key(const uint8_t *key, size_t length, uint8_t *block_key) {
    size_t block_index;

    // hash keys that are larger than a SHA1 block
    if (length > SHA1_BLOCK_SIZE) {
        sha1_hash(key, length, block_key);
        block_index = SHA1_HASH_SIZE;
    // pad keys that are smaller than a SHA1 block
    } else {
        // copy entire key to block
        memcpy(block_key, key, length);
        block_index = length;
    }

    // pad remainder of block with zeroes
    for (size_t i = block_index; i < SHA1_BLOCK_SIZE; ++i) {
        block_key[i] = 0x00;
    }
}

/*
 * XOR each element of `block_key` with `pad_value` and store in `key_pad`.
 *
 * block_key: block-length array of octets contining key
 * pad_value: value to XOR with each octet in `block_key`
 * key_pad: block-length array to store key pad in
 */
void make_pad(const uint8_t *block_key, uint8_t pad_value, uint8_t *key_pad) {
    for (size_t i = 0; i < SHA1_BLOCK_SIZE; ++i) {
        key_pad[i] = block_key[i] ^ pad_value;
    }
}

/* Set the initial state of a context. */
void hmac_sha1_init(hmac_sha1_context *context,
        const uint8_t *key, size_t length) {
    uint8_t inner_key_pad[SHA1_BLOCK_SIZE];
    sha1_context *hash_context;

    hash_context = &(context->context);

    // begin inner hash
    sha1_init(hash_context);
    make_key(key, length, context->block_key);

    // add inner key pad to digest
    make_pad(context->block_key, 0x36, inner_key_pad);
    sha1_update(hash_context, inner_key_pad, SHA1_BLOCK_SIZE);
}

/* Update a context with a message. */
void hmac_sha1_update(hmac_sha1_context *context,
        const uint8_t *message, size_t length) {
    sha1_update(&(context->context), message, length);
}

/* Calculate the digest of all messages added to a context. */
void hmac_sha1_digest(hmac_sha1_context *context, uint8_t *digest) {
    uint8_t outer_key_pad[SHA1_BLOCK_SIZE];
    sha1_context *hash_context;

    hash_context = &(context->context);

    // end inner hash
    sha1_digest(hash_context, digest);

    // calculate outer hash
    sha1_init(hash_context);
    make_pad(context->block_key, 0x5c, outer_key_pad);
    sha1_update(hash_context, outer_key_pad, SHA1_BLOCK_SIZE);
    sha1_update(hash_context, digest, SHA1_HASH_SIZE);
    sha1_digest(hash_context, digest);
}

/* Calculate the HMAC-SHA1 hash of a message. */
void hmac_sha1_hash(const uint8_t *key, size_t key_length,
        const uint8_t *message, size_t message_length,
        uint8_t *digest) {
    hmac_sha1_context context;

    hmac_sha1_init(&context, key, key_length);
    hmac_sha1_update(&context, message, message_length);
    hmac_sha1_digest(&context, digest);
}
