/*
 * Driver for SainSmart 4-channel 5V relay board.
 */

#include "relay.h"

#include <avr/io.h>

/*
 * Port where relay module is connected.
 * The lowest bits of port are relay outputs.
 */
#define RELAY_DDR DDRC
#define RELAY_PORT PORTC
#define RELAY_MASK ((1 << (RELAY_COUNT + 1)) - 1)


/* Set direction of pins and set all relays to open. */
void relay_init(void) {    
    RELAY_DDR |= RELAY_MASK; // set masked pins as output
    RELAY_PORT |= RELAY_MASK; // turn all relays off
}

/* Create a mask for use with `relay_set()` and `relay_get()`. */
uint8_t relay_select(uint8_t index) {
    return (1 << (index - 1)) & RELAY_MASK;
}

/* Close the masked relays. */
void relay_set(uint8_t mask) {
    RELAY_PORT = RELAY_PORT & ~mask;
}

/* Open the masked relays. */
void relay_clear(uint8_t mask) {
    RELAY_PORT = RELAY_PORT | mask;
}

/* Read the current state of all relays. */
uint8_t relay_read(void) {
    return ~RELAY_PORT & RELAY_MASK;
}

/* Write the state of all relays. */
void relay_write(uint8_t state) {
    RELAY_PORT = (RELAY_PORT & ~RELAY_MASK) | (~state & RELAY_MASK);
}
