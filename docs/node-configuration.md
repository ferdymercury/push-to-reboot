---
layout: page
index: 3
title: Node Configuration
description: How to configure node settings and password
---

# Node Configuration
Before any DTMF messages can be sent to a node it needs to have an HOTP
secret set via the serial interface. Connect the PTR device to a host computer
with a USB cable and launch a serial terminal to connect at 9600 baud using 8
data bits, no parity bit, and 1 stop bit. The following command connects
using [minicom](https://en.wikipedia.org/wiki/Minicom) (replace `ttyUSB0` with
the USB serial device):

```shell
$ minicom -D /dev/ttyUSB0 -b 9600 
```

Once a serial terminal is opened you should be greeted with a prompt: `>`.
Typing `help` will print a list of all settings that can be configured and
descriptions of each. The only required setting that needs to be set is
`auth_secret`; all other settings default to usable values.

To set an HOTP secret formatted as base 32 use:

```
> set auth_secret.b32 <base 32 secret>
```

To set an HOTP secret formatted as hexadecimal use:

```
> set auth_secret.hex <hexadecimal secret>
```

After a secret has been set the HOTP counter will be set to 0 and DTMF
messages will be accepted.
