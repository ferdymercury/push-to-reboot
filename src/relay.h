/*
 * Driver for SainSmart 4-channel 5V relay board.
 * The control pins for the relay board are active at low logic levels, but
 * this module accepts and converts "normal" high level logic. This module also
 * provides functions that simplify masking and control of relay pins.
 */

#ifndef RELAY_H
#define RELAY_H

#include <stdint.h>

/*
 * Count of relays attached to port.
 */
#define RELAY_COUNT 4

/*
 * Mask that addresses all relays.
 */
#define RELAY_MASK_ALL ((1 << RELAY_COUNT) - 1) 


/*
 * Set direction of pins and set all relays to open.
 * This function needs to be called before any other in the same module.
 */
void relay_init(void);

/*
 * Create a mask for use with `relay_set()` and `relay_get()`.
 *
 * index: one-based index of relay to select
 * return: a mask selecting the indicated relay
 */
uint8_t relay_select(uint8_t index);

/*
 * Close the masked relays.
 *
 * mask: mask containing 1s of relays to be closed
 */
void relay_set(uint8_t mask);

/*
 * Open the masked relays. 
 *
 * mask: mask containing 1s of relays to be opened
 */
void relay_clear(uint8_t mask);

/*
 * Read the current state of all relays.
 *
 * return: a mask containing the current state of all relays
 */
uint8_t relay_read(void);

/*
 * Write the state of all relays.
 *
 * state: a mask containing the new state of all relays
 */
void relay_write(uint8_t state);


#endif /* RELAY_H */
