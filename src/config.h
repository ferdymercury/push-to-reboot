/*
 * Device configuration structure and command processor.
 * Configuration of a node is done over a serial terminal. This module includes
 * a command processor that can access or manipulate a user-specified
 * configuration value. The command processor handles serial output, reports
 * errors as human-readable strings, and includes built-in help to simplify
 * calling code.
 */

#ifndef CONFIG_H
#define CONFIG_H

#include "hotp.h"

#include <stdint.h>
#include <stdbool.h>

/*
 * Default configuration values.
 */
#define CONFIG_DEFAULT_VALUES { \
    .node_number        = 1,    \
    .reboot_duration    = 3,    \
    .call_duration      = 1,    \
    .auth_duration      = 30    \
}


/*
 * Device parameters and state.
 */
typedef struct config_info {
    uint8_t node_number;        // node number
    uint8_t reboot_duration;    // duration of delay during a reboot in seconds
    uint8_t call_duration;      // duration of delay during a call in seconds
    uint8_t auth_duration;      // duration of authorization in seconds
    hotp_context auth_context;  // one-time password context
    uint8_t port_state;         // last set state of ports
} config_info;

/*
 * Write a command prompt to serial.
 * This function requires the serial module `serial.h` to be initialized.
 */
void config_prompt(void);

/*
 * Execute a configuration command.
 * This function accepts basic commands to access and modify configuration
 * values. All printing and error handling is handled in this function to
 * simplify calling code. This function requires the serial module `serial.h`
 * to be initialized.
 *
 * config: configuration commands will be applied to
 * command_string: string containing command and arguments
 * return: true if the command was executed successfully, false otherwise
 */
bool config_exec(config_info *config, char *command_string);

/*
 * Indicate if the authentication secret has been set.
 *
 * config: configuration to check
 * return: true if an authentication secret is set, false otherwise
 */
bool config_is_secret_set(config_info *config);


#endif /* CONFIG_H */
