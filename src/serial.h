/*
 * Async serial using hardware USART.
 * This module is used to communicate with a human via an attached serial
 * terminal. Functions are included to read and write lines of text to a dumb
 * terminal.
 */

#ifndef SERIAL_H
#define SERIAL_H

#include <avr/pgmspace.h>

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


/*
 * Set speed and mode of the UART.
 * This function needs to be called before any other in the same module.
 */
void serial_init(void);

/*
 * Indicate if a character is available to be read.
 *
 * return: true if a character is available, false otherwise
 */
bool serial_available(void);

/*
 * Read a single character from the UART.
 * This function requires the timer module `timer.h` to be initialized.
 *
 * timeout: number of milliseconds to wait for an incoming character
 * return: the read character or NUL on timeout
 */
char serial_read(uint32_t timeout);

/*
 * Read a '\r' delimited string of characters from the UART.
 * All printable characters recieved are echoed to serial output. All control
 * characters except '\b' (backspace) and '\r' (carriage return) are ignored.
 * Backspace removes the previous character from the buffer and sends "\b \b"
 * to serial output. Carriage return ends character input and sends "\r\n" to
 * serial output. Timeout or overflow discards the input buffer and returns 0.
 * This function requires the timer module `timer.h` to be initialized.
 *
 * buffer: output buffer for recieved character string
 * length: length of the output buffer
 * timeout: number of milliseconds to wait for an incoming character
 * return: the count of read characters; 0 on timeout or overflow
 */
size_t serial_read_line(char *buffer, size_t length, uint32_t timeout);

/*
 * Write a single char to the UART.
 *
 * data: char to write
 */
void serial_write(char data);

/*
 * Write a string to the UART.
 *
 * data: null terminated string to write
 */
void serial_write_string(const char *data);

/*
 * Write "\r\n" to the UART.
 */
void serial_write_newline(void);

/*
 * Write a string followed by "\r\n" to the UART.
 *
 * data: null terminated string to write
 */
void serial_write_line(const char *data);

/*
 * Write a string from program memory to the UART.
 *
 * data: string created using the PSTR macro
 */
void serial_write_p_string(PGM_P data);

/*
 * Write a string followed by "\r\n" from program memory to the UART.
 *
 * data: string created using the PSTR macro
 */
void serial_write_p_line(PGM_P data);


#endif /* SERIAL_H */
