/*
 * Test assertion macros.
 * This module provides macros for asserting the result of comparisons in
 * tests. Any assertion failure will result in a message printed to stderr and
 * immediate process exit with error code 1.
 */

#ifndef TEST_H
#define TEST_H

#include <stdio.h>
#include <stdlib.h>

/*
 * Assert that LEFT OP RIGHT is true or exit process with error message.
 *
 * LEFT: left hand operand
 * OP: comparison operator
 * RIGHT: right hand operand
 * ...: printf format string and arguments to print to stderr on failure
 */
#define TEST_ASSERT(LEFT, OP, RIGHT, ...) {             \
    if (!((LEFT) OP (RIGHT))) {                         \
        fprintf(stderr, "Error: %s:%d in %s(): ",       \
            __FILE__, __LINE__, __func__);              \
        fprintf(stderr, __VA_ARGS__);                   \
        fprintf(stderr, "\n");                          \
        exit(1);                                        \
    }                                                   \
}

/*
 * Assert that LEFT[i] OP RIGHT[i] is true for all elements or exit process
 * with error message.
 *
 * LEFT: left hand array
 * OP: element comparison operator
 * RIGHT: right hand array
 * LENGTH: count of elements to compare
 * ...: printf format string and arguments to print to stderr on failure
 */
#define TEST_ASSERT_ALL(LEFT, OP, RIGHT, LENGTH, ...) {                 \
    for (size_t test_h_i = 0; test_h_i < LENGTH; ++test_h_i) {          \
        TEST_ASSERT(LEFT[test_h_i], OP, RIGHT[test_h_i], __VA_ARGS__);  \
    }                                                                   \
}

#endif /* TEST_H */
