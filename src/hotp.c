/*
 * HMAC-based one-time password algorithm.
 */

#include "hotp.h"

#include <string.h>


/*
 * Copy counter to byte array high byte first.
 * This yields a big-endian array regardless of host processor endianess.
 *
 * counter: value of context counter
 * bytes: destination array for bytes of counter
 */
void unpack_counter(uint64_t counter, uint8_t *bytes) {
    for (size_t i = 0; i < sizeof(counter); ++i) {
        size_t ri; // reverse index (7..0)

        ri = sizeof(counter) - 1 - i;
        bytes[i] = counter >> (ri * 8);
    }
}

/*
 * Extract a 6-digit password from an HMAC-SHA1 digest.
 *
 * digest: HMAC-SHA1 digest
 * password: destination for extracted password
 */
void extract_password(const uint8_t *digest, char *password) {
    size_t offset;
    uint32_t binary_code;
    uint32_t mod_code;

    offset = digest[HMAC_SHA1_HASH_SIZE - 1] & 0x0F;
    binary_code = (uint32_t)(digest[offset] & 0x7F) << 24
        | (uint32_t)(digest[offset + 1] & 0xFF) << 16
        | (uint32_t)(digest[offset + 2] & 0xFF) << 8
        | (uint32_t)(digest[offset + 3] & 0xFF);
    mod_code = binary_code % 1000000UL;

    // format mod code as a string
    for (size_t i = 0; i < HOTP_PASSWORD_LENGTH; ++i) {
        char digit;

        digit = (mod_code % 10) + '0';
        password[HOTP_PASSWORD_LENGTH - 1 - i] = digit;
        mod_code /= 10;
    }

    password[HOTP_PASSWORD_LENGTH] = '\0';
}

/* Set the secret of a context and reset the counter to 0. */
void hotp_init(hotp_context *context,
        const uint8_t *secret, size_t length) {
    memcpy(context->secret, secret, length);
    context->secret_length = length;
    context->counter = 0;
}

/* Get the next password in the sequence and increment counter. */
void hotp_next(hotp_context *context, char *password) {
    uint8_t digest[HMAC_SHA1_HASH_SIZE];
    uint8_t counter_bytes[sizeof(context->counter)];

    unpack_counter(context->counter, counter_bytes);
    hmac_sha1_hash(
        context->secret, context->secret_length,
        counter_bytes, sizeof(context->counter),
        digest);
    extract_password(digest, password);

    ++context->counter;
}

/* Check if a password is within the synchronization window. */
bool hotp_check(hotp_context *context, const char *password) {
    uint64_t saved_counter;

    // save counter
    saved_counter = context->counter;

    // try all passwords until one is found or window is exhausted
    for (size_t i = 0; i < HOTP_LOOKAHEAD_COUNT; ++i) {
        char next_password[HOTP_PASSWORD_LENGTH + 1];

        hotp_next(context, next_password);
        if (strcmp(password, next_password) == 0) {
            return true;
        }
    }

    // restore saved counter on failure
    context->counter = saved_counter;

    return false;
}
